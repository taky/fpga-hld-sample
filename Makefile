XCXX = arm-linux-gnueabihf-g++
XCXXFLAGS = -std=c++11 -O3 -DALTERA_FPGA $(shell aocl compile-config --arm)
LIBS     = -lm $(shell aocl link-config --arm)

all: host device

host: build build/hello

device: build build/hello_world.aocx

build:
	mkdir build

build/hello: host/src/hello.cpp
	$(XCXX) $(XCXXFLAGS) -Ihost/include -o $@ $^ $(LIBS)

build/hello_world.aocx: device/hello_world.cl
	(cd build && aoc -v --board de1soc_sharedonly -o $(notdir $@) $(addprefix ../device/, $(notdir $<)))

clean:
	rm -rf build/hello build/*.o

distclean:
	rm -rf build

.PHONY: all clean distclean host device
