#include <iostream>

#define __CL_ENABLE_EXCEPTIONS
#include "cl.hpp"
#include "util.hpp"

int main(int argc, char** argv) {
    try {
        const int hw_length = 5 + 1 + 5 + 1 + 1; // "hello" + " " + "world" + "\n" + "\0"

        char ret[hw_length];

        cl::Context context(CL_DEVICE_TYPE_DEFAULT);
        cl::CommandQueue queue(context);
        cl::Program program(context, context.getInfo<CL_CONTEXT_DEVICES>(), cl::Program::Binaries { util::loadProgramBinary("hello_world.aocx") });

        cl::Buffer cl_out(context, CL_MEM_WRITE_ONLY, hw_length);

        auto hello =
            cl::make_kernel<cl::Buffer&>(program, "hello");

        hello(cl::EnqueueArgs(queue, cl::NDRange(hw_length)),
              cl_out);

        queue.enqueueReadBuffer(cl_out, CL_TRUE, 0, hw_length, ret);
        std::cout << ret;
        return 0;
    } catch (cl::Error err) {
        std::cout << "Exception\n";
        std::cerr
            << "ERROR: "
            << err.what() << " (" << err.err() << ")"
            << std::endl;
        throw;
    }
}
