__constant char hw[] = "hello world\n";

__kernel void hello(__global char *out) {
  int tid = get_global_id(0);
  out[tid] = hw[tid];
}
